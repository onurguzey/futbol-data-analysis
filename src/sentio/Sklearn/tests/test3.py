__author__ = 'aliuzun'
import matplotlib.pyplot as plt
from shapely.geometry import LineString


max_x,min_x=45.0,0.0
max_y,min_y=30.0,0.0

min_y_line=LineString([(0.0,0.0),(45.0,0.0)])
max_y_line = LineString([(0.0,30.0),(45.0,30.0)])
min_x_line=LineString([(0.0,0.0),(0.0,30.0)])
max_x_line = LineString([(45.0,0.0),(45.0,30.0)])

def check(line):
    (x1,y1),(x2,y2) = line
    cx,cy=[min_x,max_x],[min_y,max_y]
    for index,(a,b) in enumerate([(x1,y1),(x2,y2)]):
        intersect_line_min_x,intersect_line_max_x = min_x_line,max_x_line
        intersect_line_min_y,intersect_line_max_y = min_y_line,max_y_line





    # return new1x,new1y,new2x,new2y

    def getNew(a,b):
        new1x,new1y = a,b
        if a <= cx[0]:
            inter_p = intersect_line_min_x.intersection(LineString(line))
            new1x,new1y =inter_p.x,inter_p.y
            return new1x,new1y

        elif a > cx[1]:
            inter_p = intersect_line_max_x.intersection(LineString(line))
            new1x,new1y =inter_p.x,inter_p.y
            return new1x,new1y

        elif b <= cy[0] :
            inter_p = intersect_line_min_y.intersection(LineString(line))
            new1x,new1y =inter_p.x,inter_p.y
            return new1x,new1y

        elif b > cy[1]:
            inter_p = intersect_line_max_y.intersection(LineString(line))
            new1x,new1y =inter_p.x,inter_p.y
            return new1x,new1y
        else:
            return new1x,new1y



def chechLines(lines):

    new_lines=[]
    for line in lines:
        (x1,y1),(x2,y2) = line
        plt.scatter([x1,x2],[y1,y2],c='r')
        x1,y1,x2,y2=check(line)

        new_lines.append([(x1,y1),(x2,y2)])
        plt.scatter([x1,x2],[y1,y2],c='b')
    print new_lines
    # plt.plot([45,0],[45,30],c='r')
    # plt.plot([45,0],[45,30],c='b')
    # plt.plot([0,0],[45,0],c='r')
    # plt.plot([0,30],[50,30],c='r')

    im2=plt.imread('/Users/aliuzun/PycharmProjects/futbol-data-analysis/src/sentio/Sklearn/srcc/background.png',0)
    hm = plt.imshow(im2, extent=[-3.0, 50.0, -10.0, 40.0], aspect="auto")
    plt.show()

# #
line1=LineString([(-9.0,-11.0),(10.0,8.0)])
line2=LineString([(5.0,0.0),(-5.0,20.0)])
a=line2.intersection(line1)
# print a.geoms[0].coords[0]
lines=[[(-10.0,45.0),(10.0,10.0)]]
chechLines(lines)