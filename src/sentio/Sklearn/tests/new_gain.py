__author__ = 'aliuzun'
from src.sentio.Parameters import *
from src.sentio.analytics.Analyze import Analyze
import matplotlib.pyplot as plt
import math

target_loc={0:(3.0,35.0),1:(100.0,35.0)}
R=16.5
def getTarget(p,t):
    x1,y1=p
    x2,y2=target_loc[t]
    dx,dy=(x1-x2),(y1-y2)

    distance=math.sqrt(math.pow(dx,2)+math.pow(dy,2))
    if distance > R:
        distance-=R

    alpha=math.degrees(math.atan(dy/dx))
    xp,yp  = x1 + distance*round(math.cos(math.radians(alpha)),2) ,\
                           y1 + distance*round(math.sin(math.radians(alpha)),2)

    if y1 > y2: # above the line
        if x1>x2:
            xp,yp  = x1 - distance*round(math.cos(math.radians(alpha)),2),\
                           y1 - math.fabs(distance*round(math.sin(math.radians(alpha)),2))
        else:
            xp,yp  = x1 + distance*round(math.cos(math.radians(alpha)),2) ,\
                           y1 - math.fabs(distance*round(math.sin(math.radians(alpha)),2))
    else:
        if x1>x2:
            xp,yp  = x1 - distance*round(math.cos(math.radians(alpha)),2),\
                       y1 + math.fabs(distance*round(math.sin(math.radians(alpha)),2))
        else:
            xp,yp  = x1 + distance*round(math.cos(math.radians(alpha)),2) ,\
                           y1 + math.fabs(distance*round(math.sin(math.radians(alpha)),2))



    print xp,yp
    plt.scatter(x1,y1,s=30,c='red')
    plt.scatter(xp,yp,s=30,c='blue')
    plt.scatter(x2,y2,s=30,c='green')
    im2=plt.imread('/Users/aliuzun/PycharmProjects/futbol-data-analysis/src/sentio/Sklearn/srcc/background.png',0)

    hm = plt.imshow(im2, extent=[-2.0, 107.0, 72.0, 0.0], aspect="auto")

    plt.show()


print getTarget((91.0,11.0),1)








