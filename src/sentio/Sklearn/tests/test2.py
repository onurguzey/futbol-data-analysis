__author__ = 'aliuzun'
import math
aSlow,aFast=2.86,1.43
pt=0.5 # penalty time for turning 180

def relativeSpeed(V,w,alpha):
        w_in_rad,alpha_in_rad = math.radians(w),math.radians(alpha)
        return 0.5*V*(math.cos(w_in_rad)/math.cos(alpha_in_rad) + math.sin(w_in_rad)/math.sin(alpha_in_rad))

def getPenalty(V1,V2,w1,w2,alpha,p1_penalty=False,p2_penalty=False):
    t1,t2=(math.fabs(alpha-w1)/180.0)*0.5,(math.fabs(alpha-w2)/180.0)*0.5

    if p1_penalty: d1= (math.fabs(V1)/aSlow)*aFast
    else: d1=0

    if p2_penalty: d2 = (math.fabs(V2)/aSlow)*aFast
    else: d2=0

    return d1,d2

# print relativeSpeed(3,45,225)
from shapely.geometry import LineString

line1 = LineString([(0,0), (1,0), (1,1)])
line2 = LineString([(0,1), (1,1)])

a = line1.intersection(line2)
print a.y

# lines=[[(5.0,-3.0),(10.0,7.0)],[(-5.0,-5.0),(10.0,10.0)],[(45.0,30.0),(55.0,33.9)],[(10.0,36.0),(13.0,45.0)]]
