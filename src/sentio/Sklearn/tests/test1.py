__author__ = 'aliuzun'
import math

class VoronoiWithSpeed():

    def __init__(self):
        self.aSlow=2.86*2
        self.aFast=2.86

    def getDirectionInfo(self,p1,p2,V1,V2,w1,w2):
        (x1,y1),(x2,y2)=p1,p2
        distance=math.sqrt(math.pow((x1-x2),2) + math.pow((y1-y2),2))
        alpha,slope = self.getAlpha(p1,p2)
        C1p1,C1p2 = (270.0 + alpha < w1) or (w1 < 90.0 + alpha), (270.0 + alpha < w2) or (w2 < 90.0 + alpha)
        C2p1,C2p2 = (alpha - 90.0 < w1 < 90.0 + alpha), (alpha - 90.0 < w2 < 90.0 + alpha)

        taP1,taP2= self.getTurnAngle(p1,p2)
        V1r,V2r=self.relativeSpeed(V1,w1,taP1),self.relativeSpeed(V2,w2,taP2)
        if slope >= 0:
            if C1p1 and C1p2: #status = 2
                d1,d2=self.getPenalty(V1r,V2r)
            elif not C1p1 and not C1p2: #status = 3
                d1,d2=self.getPenalty(V1r,V2r,p1_penalty=True,p2_penalty=True)
            elif x1 > x2:
                if not C1p1 and C1p2: #status = 1
                    d1,d2=self.getPenalty(V1r,V2r,p1_penalty=True,p2_penalty=False)
                else: #status = 4
                    d1,d2=self.getPenalty(V1r,V2r,p1_penalty=False,p2_penalty=True)
            else:
                if not C1p2 and C1p1: #status = 1
                    d1,d2=self.getPenalty(V1r,V2r,p1_penalty=False,p2_penalty=True)
                else: #status = 4
                    d1,d2=self.getPenalty(V1r,V2r,p1_penalty=True,p2_penalty=False)

        elif slope < 0:
            if C2p1 and C2p2: #status = 2
                d1,d2=self.getPenalty(V1r,V2r)
            elif not C2p1 and not C2p2: #status = 3
                d1,d2=self.getPenalty(V1r,V2r,p1_penalty=True,p2_penalty=True)
            elif x1 > x2:
                if not C2p2 and C2p1: #status= 1
                    d1,d2=self.getPenalty(V1r,V2r,p1_penalty=False,p2_penalty=True)
                else: #status = 4
                    d1,d2=self.getPenalty(V1r,V2r,p1_penalty=True,p2_penalty=False)
            else:
                if not C2p1 and C2p2: #status= 1
                    d1,d2=self.getPenalty(V1r,V2r,p1_penalty=True,p2_penalty=False)
                else: #status =4
                    d1,d2=self.getPenalty(V1r,V2r,p1_penalty=False,p2_penalty=True)

        return distance,d1,d2

    def relativeSpeed(self,V,w,alpha):
        w_in_rad,alpha_in_rad = math.radians(w),math.radians(alpha)
        return 0.5*V*(math.cos(w_in_rad)/math.cos(alpha_in_rad) + math.sin(w_in_rad)/math.sin(alpha_in_rad))



    def getPenalty(self,V1,V2,p1_penalty=False,p2_penalty=False):
        if p1_penalty: d1= (math.fabs(V1)/self.aSlow)*self.aFast
        else: d1=0

        if p2_penalty: d2 = (math.fabs(V2)/self.aSlow)*self.aFast
        else: d2=0

        return d1,d2

    def movePoint(self,p,V1,V2,d,alpha,d_penalty):
        d1=(d/(V1+V2))*V2
        dm=d-2*(d1 - d_penalty)
        x,y=self.NewPoint(p,dm,alpha)
        return (x,y)

    def NewPoint(self,p,d,alpha):
        x0,y0=p
        Q1= math.radians(alpha)
        sings=[1.0,-1.0]
        x_sing,y_sing=1,1
        # if 0 < Q1 <=90:      x_sing,y_sing=sings[0],sings[0]
        # elif 90 < Q1 <=180:  x_sing,y_sing=sings[1],sings[0]
        # elif 180 < Q1 <=270: x_sing,y_sing=sings[1],sings[1]
        # else:                x_sing,y_sing=sings[0],sings[1]
        x1,y1  = x0 + x_sing*(d)*round(math.cos(Q1),2) ,\
                       y0 + y_sing*(d)*round(math.sin(Q1),2)
        return x1,y1

    def getTurnAngle(self,p1,p2):
        alpha,slope = self.getAlpha(p1,p2)
        print slope
        if slope >= 0 and p1[0]>=p2[0]: taP1,taP2= alpha+180,alpha
        elif slope >= 0 and p1[0]< p2[0]: taP1,taP2= alpha,alpha+180
        elif slope <= 0 and p1[0]>=p2[0]: taP1,taP2= alpha,alpha+180
        elif slope <= 0 and p1[0]<p2[0]: taP1,taP2= alpha+180,alpha
        return taP1,taP2

    def getPairs(self,p1,p2,V1,V2,w1,w2):
        taP1,taP2=self.getTurnAngle(p1,p2)
        V1r,V2r=self.relativeSpeed(V1,w1,taP1),self.relativeSpeed(V2,w2,taP2)
        distance,d1_penalty,d2_penalty=self.getDirectionInfo(p1,p2,V1,V2,w1,w2)
        if V1r >= V2r: point,dp,Vm,Vo=p1,d1_penalty,V1,V2
        else: point,dp,Vm,Vo=p2,d2_penalty,V2,V1
        return (point,distance,Vm,Vo,dp)

    def getAlpha(self,p1,p2):
        (x1,y1),(x2,y2)=p1,p2
        dx,dy=(x2-x1),(y2-y1)
        dx =(dx if dx !=0 else 0.01)
        slope = math.atan(dy/dx)
        alpha = math.degrees(slope)
        alpha = (alpha if alpha > 0 else alpha + 180.0)
        return (alpha,slope)

    def Do(self,p1,p2,p3,V1,V2,V3,w1,w2,w3):
        info,check_list=[],[]
        pairs=[(p1,p2),(p1,p3),(p2,p3)]
        Vs=[(V1,V2),(V1,V3),(V2,V3)]
        ws=[(w1,w2),(w1,w3),(w2,w3)]
        p_average,p_stay,case=None,None,None
        for index in range(len(pairs)):
            (pa,pb),(Va,Vb),(wa,wb) = pairs[index],Vs[index],ws[index]
            (p,distance,Vm,Vo,dp),alpha = self.getPairs(pa,pb,Va,Vb,wa,wb),self.getAlpha(pa,pb)[0]
            info.append((p,distance,alpha,Vm,Vo,dp))
            if p not in check_list: check_list.append(p)
            elif p in check_list:
                case=2
                check_list.append(p)
            if len(check_list)==3 and case!=2:
                p_stay = check_list.pop(-1)
                p_average=check_list[-1]
        if case==2:
            for p in [p1,p2,p3]:
                if p not in check_list: p_stay=p


        test_d={}
        for p,distance,alpha,Vm,Vo,dp in info:
            x,y=self.movePoint(p,Vm,Vo,distance,alpha,dp)
            if p not in test_d:
                test_d[p]=(x,y)
            else:
                xp,yp=test_d[p]
                test_d[p]=((x+xp)/2.0,(y+yp)/2.0)
            if case==1 and p_average==p:
                x,y=(x+p_average[0])/2.0,(y+p_average[1])/2.0
                test_d[p]=(x,y)
        new_points=test_d.values()
        new_points.append(p_stay)
        print case
        return new_points




def getDxy(p1,p2):
    (x1,y1),(x2,y2)=p1,p2
    dx,dy=(x2-x1),(y2-y1)
    return dx,dy



p1,p1e=(10.0,10.0),(15.0,15.0)
p2,p2e=(20.0,10.0),(15.0,25.0)
p3,p3e=(15.0,24.0),(15.0,20.0)
V1=math.sqrt(math.pow((p1[0]-p1e[0]),2) + math.pow((p1[1]-p1e[1]),2))
V2=math.sqrt(math.pow((p2[0]-p2e[0]),2) + math.pow((p2[1]-p2e[1]),2))
V3=math.sqrt(math.pow((p3[0]-p3e[0]),2) + math.pow((p3[1]-p3e[1]),2))

import matplotlib.pyplot as plt


if __name__=="__main__":
    q=VoronoiWithSpeed()
    w1,w2,w3=q.getAlpha(p1,p1e)[0],q.getAlpha(p2,p2e)[0],q.getAlpha(p3,p3e)[0]
    pa,pb,pc= q.Do(p1,p2,p3,V1,V2,V3,w1,w2,w3)
    # plt.scatter([pa[0],pb[0],pc[0]],[pa[1],pb[1],pc[1]],c="r")
    print(pa,pb,pc)
    plt.scatter(p1[0],p1[1],c="b")
    plt.scatter(p2[0],p2[1],c="r")
    plt.scatter(p3[0],p3[1],c="g")

    plt.scatter(pa[0],pa[1],c="b")
    plt.scatter(pb[0],pb[1],c="r")
    plt.scatter(pc[0],pc[1],c="g")

    # plt.plot([p1[0],p2[0]],[p1[1],p2[1]],c='r')
    # plt.plot([p1[0],p3[0]],[p1[1],p3[1]],c='r')
    # plt.plot([p2[0],p3[0]],[p2[1],p3[1]],c='r')

    dx,dy=getDxy(p1,p1e)
    plt.arrow(p1[0],p1[1],dx,dy,head_length=0.1, fc='k', ec='k')
    dx,dy=getDxy(p2,p2e)
    plt.arrow(p2[0],p2[1],dx,dy,head_length=0.1, fc='k', ec='k')
    dx,dy=getDxy(p3,p3e)
    plt.arrow(p3[0],p3[1],dx,dy,head_length=0.1, fc='k', ec='k')



    im2=plt.imread('/Users/aliuzun/PycharmProjects/futbol-data-analysis/src/sentio/Sklearn/srcc/background.png',0)
    hm = plt.imshow(im2, extent=[-2.0, 107.0, 0.0, 72.0], aspect="auto")
    plt.show()



