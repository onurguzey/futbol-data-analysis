__author__ = 'aliuzun'
import numpy as np
import random
import sklearn
from sklearn.datasets.samples_generator import make_regression
import pylab
from scipy import stats
from SurveyScoreCalculation import ScoreCal
w=ScoreCal()

def gradient_descent(alpha, x,y,pair, ep=0.0001, max_iter=10000):

    converged = False
    iter = 0
    y=33
    m = x.shape[0] # number of samples

    # initial theta
    # t0 = np.random.random(x.shape[1])
    # t1 = np.random.random(x.shape[1])[0]
    # t2 = np.random.random(x.shape[1])[0]
    # t3 = np.random.random(x.shape[1])[0]
    # t4 = np.random.random(x.shape[1])[0]
    # t5 = np.random.random(x.shape[1])[0]
    # t6 = np.random.random(x.shape[1])[0]
    # t7 = np.random.random(x.shape[1])[0]
    # t8 = np.random.random(x.shape[1])[0]

    # print t1,t2,t3,t4,t5,t6,t7
    # total error, J(theta)
    t1,t2,t3,t4,t5,t6,t7=10.0, 3.0, 12.0, 36, 82, 25, 82
    print t1,t2,t3,t4,t5,t6,t7
    print w.CostScore2([t1,t2,t3,t4,t5,t6,t7],pair),"ww"
    score=w.CostScore2([t1,t2,t3,t4,t5,t6,t7],pair)[0]
    # J = sum([(w.CostScore2([t1,t2,t3,t4,t5,t6,t7],pair) - y)**2 for i in range(m)])

    J = sum([(score - y)**2 for i in range(m)])

    # print "j",J
    # Iterate Loop
    while not converged:

        # for each training sample, compute the gradient (d/d_theta j(theta))
        # grad0 = 1.0/m * sum([(ScoreCal.CostScore2([t1,t2,t3,t4,t5,t6,t7,t8],pair) - y) for i in range(m)])
        score=w.CostScore2([t1,t2,t3,t4,t5,t6,t7],pair)[0]
        grad1 = 1.0/m * sum([(score - y)*x[i] for i in range(m)])
        grad2 = 1.0/m * sum([(score - y)*x[i] for i in range(m)])
        grad3 = 1.0/m * sum([(score - y)*x[i] for i in range(m)])
        grad4 = 1.0/m * sum([(score - y)*x[i] for i in range(m)])
        grad5 = 1.0/m * sum([(score - y)*x[i] for i in range(m)])
        grad6 = 1.0/m * sum([(score - y)*x[i] for i in range(m)])
        grad7 = 1.0/m * sum([(score - y)*x[i] for i in range(m)])
        # grad8 = 1.0/m * sum([(w.CostScore2([t1,t2,t3,t4,t5,t6,t7],pair) - y)*x[i] for i in range(m)])

        # update the theta_temp
        # temp0 = t0 - alpha * grad0
        temp1 = t1 - alpha * grad1
        temp2 = t2 - alpha * grad2
        temp3 = t3 - alpha * grad3
        temp4 = t4 - alpha * grad4
        temp5 = t5 - alpha * grad5
        temp6 = t6 - alpha * grad6
        temp7 = t7 - alpha * grad7
        # temp8 = t8 - alpha * grad8

        # update theta
        # t0 = temp0
        t1,t2,t3,t4,t5,t6,t7 = temp1,temp2,temp3,temp4,temp5,temp6,temp7

        # mean squared error
        print w.CostScore2([t1,t2,t3,t4,t5,t6,t7],pair)
        score=w.CostScore2([t1,t2,t3,t4,t5,t6,t7],pair)[0]
        e = sum( [ (score - y)**2 for i in range(m)] )

        print J,e,ep
        if abs(J-e) <= ep:
            print 'Converged, iterations: ', iter, '!!!'
            converged = True

        J = e   # update error
        iter += 1  # update iter

        if iter == max_iter:
            print 'Max interactions exceeded!'
            converged = True
        print t1,t2,t3,t4,t5,t6,t7
        return t1,t2,t3,t4,t5,t6,t7

if __name__ == '__main__':

    x, y = make_regression(n_samples=100, n_features=1, n_informative=1,
                        random_state=0, noise=35)

    # for i in range(10):
    #     print np.random.randint(x.shape[0])
    print 'x.shape = %s y.shape = %s' %(x.shape, y.shape)

    alpha = 0.01 # learning rate
    ep = 0.01 # convergence criteria
    print gradient_descent(alpha, x,y, 9,ep, max_iter=1000)
    # call gredient decent, and get intercept(=theta0) and slope(=theta1)
    # theta0, theta1 = gradient_descent(alpha, x, y, ep, max_iter=1000)
    # print ('theta0 = %s theta1 = %s') %(theta0, theta1)
    #
    # # check with scipy linear regression
    # slope, intercept, r_value, p_value, slope_std_error = stats.linregress(x[:,0], y)
    # print ('intercept = %s slope = %s') %(intercept, slope)
    #
    # # plot
    # for i in range(x.shape[0]):
    #     y_predict = theta0 + theta1*x
    #
    # pylab.plot(x,y,'o')
    # pylab.plot(x,y_predict,'k-')
    # pylab.show()
    # print "Done!"