import random
from SurveyAnalysis.analyse2 import *

survey_answer = [['P3', 'P2', 'P1'], ['P2', 'P3', 'P1'], ['P2', 'P1', 'P3'], ['P3', 'P2', 'P1'],
                      ['P2', 'P1', 'P3'], (['P1', 'P2', 'P3'], ['P3', 'P2', 'P1']), ['P2', 'P1', 'P3'],
                      ['P2', 'P1', 'P3'], ['P2', 'P3', 'P1'], ['P2', 'P3', 'P1'], ['P2', 'P3', 'P1'],
                      (['P3', 'P1', 'P2'], ['P1', 'P3', 'P2'])]


# survey_answer = [['P3', 'P2', 'P1'], ['P2', 'P3', 'P1'], ['P2', 'P1', 'P3'], ['P3', 'P2', 'P1'],
#         ['P2', 'P1', 'P3'], (['P1', 'P2', 'P3'], ['P3', 'P2', 'P1']), ['P2', 'P1', 'P3'],
#         ['P2', 'P1', 'P3'], ['P2', 'P3', 'P1'], ['P2', 'P3', 'P1'], ['P2', 'P3', 'P1'],
#         ['P3', 'P1', 'P2']]

file_wr = csv.writer(open('C:\\Users\\Dexter\\Desktop\\futbol-data-analysis\\SurveyAnalysis\\optimization_weight_data.csv', "a"),
            delimiter='\t', quoting=csv.QUOTE_NONE)


def CostScore2(solution,Q):
    a = SurveyAnalyse(solution,Q)
    a.Write_to_csv()
    a.sortPossitions()
    our_answer = a.get_result_list()
    k = 0
    tmp_list = []
    # try:

    while k < 12:
        if Q-1 != k:
            if k == 5 or k==11:
                sa1, sa2 = survey_answer[k][0], survey_answer[k][1]
                score1 = getScore(sa1, our_answer[k])
                score2 = getScore(sa2, our_answer[k])
                # print max(score1,score2)
                tmp_list.append(max(score1, score2))
            else:

                score = getScore(survey_answer[k], our_answer[k])
                tmp_list.append(score)
        k += 1
    return sum(tmp_list)


def getScore(survey_answer,our_answer):
    survey_pair=[(survey_answer[0],survey_answer[1]),(survey_answer[0],survey_answer[2]),(survey_answer[1],survey_answer[2])]
    tool_pair=[(our_answer[0],our_answer[1]),(our_answer[0],our_answer[2]),(our_answer[1],our_answer[2])]
    score=0
    # print survey_pair,"s"
    # print tool_pair,"t"
    for pair in tool_pair:
        if pair in survey_pair:
            score=score+1

    return score



def geneticoptimize(Q,popsize=50 ,file_wr=file_wr,step=1,mutprob=0.2 ,elite=0.2 ,maxiter=50):
    #domain = [(1, 100), (1, 100), (1, 100), (1, 100)]  # [r1,r2,rg,w1,w2,w3,w4]
    #domain = [(1, 10), (1, 5), (1, 20) ] #, (1, 100), (1, 100), (1, 100), (1, 100)]
    domain = [(1, 10), (1, 5), (1, 20) , (1, 100), (1, 100), (1, 100), (1, 100)]

    # Mutation Operation
    def mutate(vec):
        i=random.randint(0,len( domain)-2)
        vecb = []
        if random.random()<0.5 and vec[i]>domain[i ] [0]:
            vecb = vec[0:i]+[vec[i]- step]+vec [ i+1:]
        elif vec[i]<domain[i] [ 1]:
            vecb = vec[0:i]+[vec[i]+ step]+vec[ i +1:]
        else:
            vecb = vec
        # print 'mutate: ', vecb, vec, i
        return vecb

    # Crossover Operation
    def crossover(r1,r2):
        i=random.randint(1,len(domain)- 2)
        return r1[ 0:i]+r2[i:] # Build the initial population

    pop=[] # print 'psize', popsize
    for i in range(popsize):
        vec=[random.randint(domain[i][0], domain[i][1])
             for i in range(len(domain))]
        pop.append(vec)

    # How many winners from each generation?
    topelite = int(elite * popsize)

    # How many winners from each generation?
        # Main loop
    for i in range(maxiter):
        # for j in range(len(pop)):
        #     print j, pop[j]

        scores=[(CostScore2(v,Q),v) for v in pop]
        scores. sort(reverse=True)
        ranked=[v for (s,v) in scores] # Start with the pure winners
        pop = ranked[0:topelite] # Add mutated and bred forms of the winners

        while len(pop)<popsize:
            if random.random() < mutprob:

                # Mutation
                c = random.randint(0, topelite - 1)
                pop.append(mutate(ranked[c]))
            else:
                # Crossover
                c1 = random.randint(0, topelite - 1)
                c2 = random.randint(0, topelite - 1)
                pop.append(crossover(ranked[c1], ranked[c2]))

        print scores[0][0]

    return scores[0][ 1 ],scores[0][0]


for i in range(1 ,13):
    print "Q:",i,"----------------------------------------"
    print geneticoptimize(i)

