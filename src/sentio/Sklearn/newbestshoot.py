__author__ = 'aliuzun'
def getLN(self, p1, p2s, Gxy): # give the number of times next position will be got
    dists=list()
    x1, y1 = p1.get_position()
    x3, y3 = Gxy
    dx = (0.01 if (x3-x1) == 0 else (x3-x1))
    tmp_slope=(y3-y1)/dx


    Q1 = math.degrees(math.atan(tmp_slope)) # Q1 is the angle of p1 - goalkeeper line
    dx = (0.01 if (x3-x1) == 0 else (x3-x1))
    slope = (y3 - y1) / dx
    a,b,c = slope,-1,( ( slope * (-x1) ) + y1 )

    for p2 in p2s:
        x2, y2 = p2.get_position()
        dx = (0.01 if (x2-x1) == 0 else (x2-x1))
        angle1 = math.degrees(math.atan((y2-y1)/dx)) # the angle of p1-p2 line
        alpha_p1 = 2*math.fabs(Q1-angle1) # the angle with p1's direction and p1-p2 line
        d2 = math.fabs(a * x2 + b * y2 + c) / math.sqrt(math.pow(a, 2) + math.pow(b, 2))

        if alpha_p1 > 180:
            # alpha_p1 = alpha_p1 - 180
            t=(d2/average_speed_player)
        else:
            alpha_p1 = math.radians(alpha_p1)
            t=(d2/average_speed_player)*math.sin(alpha_p1)

        dists.append(int(t))
    # print dists ##########################
    return dists


def getPointLocation(self, p1, p2, Gxy):
    x3,y3=Gxy
    x1,y1=p1.get_position()
    x2,y2=p2.get_position()
    dx = (0.01 if (x3-x1) == 0 else (x3-x1))
    slope=(y3-y1)/dx


    a=y1-slope*x1 # constant
    tmp_val=slope*x2+a

    if y2 > tmp_val:   return -1
    elif y2 < tmp_val: return 1
    else:              return 0


def getFutureCoordinates(self, p1, p2s, Gxy, t,Q1,Q2s):
    signs = [1.0, -1.0]
    x1, y1 = p1.get_position()
    x3, y3 = Gxy

    if x1 > x3:
        x1_p,y1_p  = x1 - (average_distance_per_frame)*round(math.cos(math.radians(Q1)),2) ,\
                   y1 - (average_distance_per_frame)*round(math.sin(math.radians(Q1)),2)
    else:
        x1_p,y1_p  = x1 + (average_distance_per_frame)*round(math.cos(math.radians(Q1)),2) ,\
                       y1 + (average_distance_per_frame)*round(math.sin(math.radians(Q1)),2)

    ## set the new coordinates


    cords=[]
    i=0
    for i in range(len(p2s)):
        p2=p2s[i]
        x2, y2 = p2.get_position()
        if Q2s==None:
            dx = (0.01 if (x2-x1) == 0 else (x2-x1))
            beta = math.degrees(math.atan((y2-y1)/dx)) # the angle of p1-p2 line
            if beta <0: beta+=360
            Q = 2*beta-Q1
            if Q<0: Q+=360
            self.Qs.append(Q)
        else:
            Q=self.Qs[i]

        print p2.getJerseyNumber(),Q
        if 0 < Q <= 90:
            x_sign, y_sign = signs[0], signs[1]
        elif 90 < Q <= 180:
            x_sign, y_sign = signs[1], signs[1]
        elif 180 < Q <= 270:
            x_sign, y_sign = signs[1], signs[0]
        else:
            x_sign, y_sign = signs[0], signs[0]

        x2_p,y2_p = x2 + x_sign*(t*average_distance_per_frame)*round(math.cos(Q),2), y2 + y_sign*(t*average_distance_per_frame)*math.fabs(round(math.sin(Q),2))



        cords.append((x2_p,y2_p))
        i+=1
       ## set the new coordinates
        p1.set_position((x1_p,y1_p))
        p2.set_position((x2_p,y2_p))
    # print len(cords),"coords"
    return ([x1_p,y1_p],cords)

