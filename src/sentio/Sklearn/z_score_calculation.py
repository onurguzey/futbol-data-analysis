from src.sentio.Sklearn.SurveyScoreCalculation import *
import csv
import glob
from SurveyAnalysis.analyse2 import *
import numpy as np


solution = [10, 3, 9, 14, 100, 68, 9]

sl =[7, 46, 24, 6] # ws for r calculation

sol =[[10, 3, 4, 5, 96, 62, 10],
[10, 5, 9, 13, 84, 29, 19],
[10, 5, 4, 4, 82, 31, 13],
[10, 3, 9, 18, 96, 94, 8],
[10, 5, 9, 6, 74, 28, 7] ,
[10, 5, 9, 28, 86, 41, 2] ,
[10, 3, 4, 20, 97, 88, 4] ,
[10, 5, 9, 24, 74, 88, 3] ,
[10, 5, 11, 12, 32, 29, 4] ,
[10, 5, 9, 40, 97, 26, 1]   ,
[10, 3, 4, 12, 88, 90, 2] ,
[10, 3, 9, 3, 82, 31, 17] ]

tts={}

#def get_mean_std(solution,Q):
#sq=SurveyAnalyse(solution,Q)


overallRisk_list, gain_list, pass_advantages_list,pa_player_list, goalChance_list,desicionTime_list, effectiveness_list = [],[],[],[],[],[],[]
for ii in range(1,13):
    LQ = ii

    for file_path in glob.glob('/Users/aliuzun/PycharmProjects/futbol-data-analysis/SampleScenarios/Positions/*.csv'):
        Q=file_path[len(file_path)-8:len(file_path)-4]

        snapShot = SnapShot(file_path)
        teams = snapShot.loadTeams()
        defined_passes = snapShot.getLoadedPassesFor(teams)
        pas = Pass()
        pas.teams = teams

        if len(str(LQ))>1: Qs=["Q"+str(LQ)+"a","Q"+str(LQ)+"b","Q"+str(LQ)+"c"]
        else: Qs=["Q0"+str(LQ)+"a","Q0"+str(LQ)+"b","Q0"+str(LQ)+"c"]

        if Q not in Qs:
            for pass_event in defined_passes:
                p1, p2 = pass_event.pass_source, pass_event.pass_target
                overallRisk, gain, pass_advantages,pa_player, goalChance,desicionTime, effectiveness = pas.effectiveness_withComponents(p1,p2,solution)#readFile=status,weight_optimization=True,Ws=self.weight_coefficient)
                overallRisk_list.append(overallRisk)
                gain_list.append(gain)
                pass_advantages_list.append(pass_advantages)
                pa_player_list.append(pa_player)
                goalChance_list.append(goalChance)
                desicionTime_list.append(desicionTime)
                effectiveness_list.append(effectiveness)


    w1s, w1m = np.std(gain_list),np.mean(gain_list)
    w2s, w2m = np.std(pass_advantages_list), np.mean(pass_advantages_list)
    w3s, w3m = np.std(goalChance_list), np.mean(goalChance_list)
    w4s, w4m = np.std(desicionTime_list), np.mean(desicionTime_list)
    tts.setdefault(LQ,[(w1m,w1s),(w2m,w2s),(w3m,w3s),(w4m,w4s)])

    print LQ,[(w1m,w1s),(w2m,w2s),(w3m,w3s),(w4m,w4s)]




print tts


def z_score(Q,tts,overallRisk, gain, pass_advantages,pa_player, goalChance,desicionTime, effectiveness):
    Ws=tts[Q]
    overallRisk, gain, pass_advantages, pa_player, goalChance, desicionTime, effectiveness=overallRisk,\
                                                                                           (gain-(Ws[0][0]))/Ws[0][1],(pass_advantages-Ws[1][0])/Ws[1][1],\
                                                                                           pa_player\
                                                                                            ,(goalChance-Ws[2][0])/Ws[2][1],\
                                                                                           (desicionTime-Ws[3][0])/Ws[3][1],\
                                                                                           effectiveness

    return overallRisk, gain, pass_advantages, pa_player, goalChance, desicionTime, effectiveness

print z_score(1,tts,0,0,0,0,0,0,0)

#
# print "Overall Risk :",np.mean(overallRisk_list),np.std(overallRisk_list)
# print "Gain :",np.mean(gain_list), np.std(gain_list)
# print "Pass Advantages :",np.mean(pass_advantages_list), np.std(pass_advantages_list)
# print "Pass Player :",np.mean(pa_player_list), np.std(pa_player_list)
# print "Goal Chance :",np.mean(goalChance_list), np.std(goalChance_list)
# print "Desicion Time :",np.mean(desicionTime_list), np.std(desicionTime_list)
# print "Effectivenes Score",np.mean(effectiveness_list), np.std(effectiveness_list)


mn_std = {1: [(-697.6508020430033, 2345.6795079163921), (57.665787871534967, 12.887442968192333),
                           (10.155344587728347, 8.3814248344722397), (1.5664418790093779, 1.0391555701532693)],
                       2: [(-697.65080204300341, 2345.6795079163921), (57.665787871534981, 12.887442968192332),
                           (10.155344587728349, 8.3814248344722397), (1.5664418790093784, 1.0391555701532695)],
                       3: [(-697.6134383281003, 2345.6905995779098), (57.391237136375103, 13.424729659449502),
                           (10.287915489040401, 8.3362846515192395), (1.5577974993026336, 1.0394741990359637)],
                       4: [(-697.5947564706488, 2345.6961451658185), (57.253961768795165, 13.683399538678872),
                           (10.35420093969643, 8.3128298596731067), (1.5534753094492619, 1.0396065227895206)],
                       5: [(-697.65220291045227, 2345.6789678013156), (56.953215122225792, 13.924968685362645),
                           (10.206855065421324, 7.9192379010776888), (1.5551093487278784, 1.0432273403311081)],
                       6: [(-697.80520178415975, 2345.632796374779), (56.975221547142418, 14.108473068178579),
                           (10.365647835306028, 7.9595156926303963), (1.5206713634429525, 1.0095378978895531)],
                       7: [(-697.71359382937101, 2345.6602254788149), (56.854950239507687, 14.229501283253045),
                           (10.354610250783876, 8.0211532975026447), (1.5146800460998875, 1.0099816781049467)],
                       8: [(-704.94540862790961, 2356.9089622157526), (56.669264190926391, 14.32258574207362),
                           (10.394816456763492, 8.1104130355493069), (1.5172976939989464, 1.0221772994453482)],
                       9: [(-625.57887357359095, 2232.0166594558445), (56.566564636273171, 14.385166758209769),
                           (10.503485561838859, 8.0991741452841417), (1.5333376623072084, 1.0168509304225521)],
                       10: [(-632.80037840644241, 2243.8575629149054), (56.539425525509522, 14.427137921946127),
                            (10.47891966626889, 8.1344635636843865), (1.5363361585546156, 1.0208415297147828)],
                       11: [(-638.70511971824021, 2253.4640352578849), (56.559551474628229, 14.473499765522734),
                            (10.516622190065648, 8.152103210726132), (1.5329121293410133, 1.0245407963467053)],
                       12: [(-643.62505020944775, 2261.4135551110371), (56.540234158515901, 14.526920870517706),
                            (10.536837659738024, 8.1649246089802165), (1.5326037643824133, 1.026673002760002)]}






