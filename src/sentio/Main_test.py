# coding=utf-8
import time as tm
import os
from src.sentio.Parameters import DATA_BASE_DIR
from src.sentio.file_io.Parser import Parser
from src.sentio.file_io.Writer import Writer
from src.sentio.file_io.reader.CSVreader import CSVreader
from src.sentio.file_io.reader.JSONreader import JSONreader
from src.sentio.file_io.reader.XMLreader import XMLreader
from src.sentio.object.Match import Match

from src.sentio.file_io.WriterOld import WriterOld


__author__ = 'emrullah'

def main():
    start = tm.time()

    coord_data = os.path.join(DATA_BASE_DIR, 'input/GS_FB_Sentio.txt')
    event_data = os.path.join(DATA_BASE_DIR, 'input/GS_FB_Event.txt')


    # reader = XMLreader(os.path.join(DATA_BASE_DIR, 'output/sentio_data.xml'))
    # game_instances, slider_mapping = reader.parse()

    # writer = Writer(game_instances, slider_mapping)
    # writer.createFileAsXML()
    # writer.createFileAsJSON()

    parser = Parser()
    parser.parseSentioData(coord_data, event_data)

    writer = WriterOld(parser.getRevisedCoordinateData(), parser.getGameEvents())
    writer.createFileAsXML()





    # match = Match(reader)
    #
    # match.buildMatchObjects()
    # match.computeEventStats()
    #
    # team_players = match.getHomeTeam().getTeamPlayers()
    #
    # # for half in home_goal_keeper.coord_info:
    # #     for milliseconds in home_goal_keeper.coord_info[half]:
    # #         print half, milliseconds, home_goal_keeper.coord_info[half][milliseconds]
    #
    # # print home_goal_keeper.computeRunningDistance()
    # for team_player in team_players:
    #     print team_player.printStats()
    #     print "---------------------"









    end = tm.time()
    print
    print end - start

if __name__ == "__main__":
    main()


