import traceback
from src.sentio.Parameters import FOOTBALL_FIELD_MIN_X, FOOTBALL_FIELD_MAX_X, \
    FOOTBALL_FIELD_MAX_Y, FOOTBALL_FIELD_MIN_Y, max_speed_player, DEFAULT_ACCELERATION
import math
from shapely import geometry
import numpy as np
from scipy.spatial import Delaunay
from shapely.ops import cascaded_union, polygonize
from descartes import PolygonPatch

__author__ = 'emrullah'


class DominantRegion:

    def __init__(self, ax):
        self.ax = ax
        self.regions = []


    def alpha_shape(self, points, alpha):
        """
        Compute the alpha shape (concave hull) of a set of points.
        @param points: Iterable container of points.
        @param alpha: alpha value to influence the gooeyness of the border. Smaller numbers
            don't fall inward as much as larger numbers. Too large, and you lose everything!
        """
        if len(points) < 4:
            # When you have a triangle, there is no sense in computing an alpha shape.
            return geometry.MultiPoint(list(points)).convex_hull

        def add_edge(edges, edge_points, coords, i, j):
            """
            Add a line between the i-th and j-th points, if not in the list already
            """
            if (i, j) in edges or (j, i) in edges:
                return  # already added
            edges.add((i, j))
            edge_points.append(coords[[i, j]])

        coords = np.array([point.coords[0] for point in points])
        tri = Delaunay(coords)
        edges, edge_points = set(), []
        # ia, ib, ic = indices of corner points of the triangle
        for ia, ib, ic in tri.vertices:
            pa, pb, pc = coords[ia], coords[ib], coords[ic]
            # Lengths of sides of triangle
            a = math.sqrt((pa[0] - pb[0]) ** 2 + (pa[1] - pb[1]) ** 2)
            b = math.sqrt((pb[0] - pc[0]) ** 2 + (pb[1] - pc[1]) ** 2)
            c = math.sqrt((pc[0] - pa[0]) ** 2 + (pc[1] - pa[1]) ** 2)
            # Semiperimeter of triangle
            s = (a + b + c) / 2.0
            # Area of triangle by Heron's formula
            area = math.sqrt(s * (s - a) * (s - b) * (s - c))
            circum_r = a * b * c / (4.0 * area)
            # Here's the radius filter.
            if circum_r < 1.0 / alpha:
                add_edge(edges, edge_points, coords, ia, ib)
                add_edge(edges, edge_points, coords, ib, ic)
                add_edge(edges, edge_points, coords, ic, ia)
        m = geometry.MultiLineString(edge_points)
        triangles = list(polygonize(m))
        return cascaded_union(triangles), edge_points


    def draw(self, visual_players):
        coordinates_per_player = {}
        for y in range(int(FOOTBALL_FIELD_MIN_Y), int(FOOTBALL_FIELD_MAX_Y) + 1, 1):
            for x in range(int(FOOTBALL_FIELD_MIN_X), int(FOOTBALL_FIELD_MAX_X) + 1, 1):
                min_time = 99999999
                dominant_player = None
                for temp_player in visual_players:
                    coord_x, coord_y = temp_player.get_position()

                    distance = math.hypot(coord_x - x, coord_y - y)
                    target_direction = math.degrees(math.atan2(y - coord_y, x - coord_x)) % 360

                    temp_speed = temp_player.speed
                    if temp_speed <= 0:
                        temp_speed = 0.1
                    elif temp_speed > max_speed_player:
                        temp_speed = max_speed_player

                    q = (temp_player.direction - target_direction) % 360
                    angle_diff = min([q, 360 - q])

                    """
                    if temp_player.jersey_number == 88:
                        print temp_player.get_position(), x, y
                        print distance
                        print target_direction
                        print temp_player.direction

                        print temp_speed
                        print angle_diff

                        print self.calculate_time(temp_speed, distance, DEFAULT_ACCELERATION)
                        print self.calculate_penalty_time(temp_speed, DEFAULT_ACCELERATION, angle_diff)
                        print "--------"
                    """
                    temp_time = self.calculate_time(temp_speed, distance, DEFAULT_ACCELERATION) + \
                                self.calculate_penalty_time(temp_speed, DEFAULT_ACCELERATION, angle_diff)
                    if temp_time < min_time:
                        min_time = temp_time
                        dominant_player = temp_player

                if dominant_player:
                    if dominant_player in coordinates_per_player:
                        coordinates_per_player[dominant_player].append(geometry.Point(x, y))
                    else:
                        coordinates_per_player[dominant_player] = [geometry.Point(x, y)]
                else:
                    print "The point is not assigned!"

        # plotting side
        for player in coordinates_per_player:
            try:
                points = coordinates_per_player[player]
                concave_hull, edge_points = self.alpha_shape(points, alpha=0.4)
                poly_patch = PolygonPatch(concave_hull.buffer(0), alpha=0.4, color=player.getObjectColor(), fill=True)
                self.ax.add_patch(poly_patch)
                self.regions.append(poly_patch)
            except:
                print "polygon not valid for %s" % player.jersey_number
                print traceback.print_exc()


    def remove(self):
        if self.regions:
            for region in self.regions:
                region.remove()
            self.regions = []


    def update(self, visual_idToPlayers):
        self.remove()
        self.draw(visual_idToPlayers.values())


    @staticmethod
    def calculate_time(initial_velocity, distance, acceleration):
        # a*x**2 + b*x + c = 0
        # (acceleration * time * time) / 2 + (initial_velocity * time) - distance = 0

        a = acceleration / 2
        b = initial_velocity
        c = -distance

        d = b ** 2 - 4 * a * c  # discriminant

        if d < 0:
            print ("This equation has no real solution")
        elif d == 0:
            x = (-b + math.sqrt(b ** 2 - 4 * a * c)) / 2 * a
            # print ("This equation has one solutions: "), x
            return x
        else:
            x1 = (-b + math.sqrt((b ** 2) - (4 * (a * c)))) / (2 * a)
            x2 = (-b - math.sqrt((b ** 2) - (4 * (a * c)))) / (2 * a)
            # print ("This equation has two solutions: ", x1, " or", x2)
            if x1 > 0:
                return x1
            elif x2 > 0:
                return x2
            print "Both of the solutions are negative"


    @staticmethod
    def calculate_penalty_time(initial_velocity, deceleration, angle_diff):
        vy = initial_velocity * math.sin(math.radians(angle_diff))
        penalty_time = DominantRegion.calculate_speed_penalty_time(vy, deceleration)
        if angle_diff >= 180:
            vx = initial_velocity * math.cos(math.radians(angle_diff))
            penalty_time += DominantRegion.calculate_speed_penalty_time(vx, deceleration)
        return penalty_time


    @staticmethod
    def calculate_speed_penalty_time(initial_velocity, deceleration):
        if initial_velocity == 0:
            return 0
        penalty_distance = (initial_velocity ** 2) / (2 * deceleration)
        penalty_time = DominantRegion.calculate_time(initial_velocity, penalty_distance, DEFAULT_ACCELERATION)
        return penalty_time
