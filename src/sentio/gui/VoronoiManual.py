from src.sentio.Parameters import FOOTBALL_FIELD_MIN_X, FOOTBALL_FIELD_MAX_X, \
    FOOTBALL_FIELD_MAX_Y, FOOTBALL_FIELD_MIN_Y
from matplotlib.patches import Polygon
import math
from shapely import geometry

__author__ = 'emrullah'


class VoronoiManual:

    def __init__(self, ax):
        self.ax = ax
        self.regions = []

    def draw(self, visual_players):
        coordinates_per_player = {}
        for y in range(int(FOOTBALL_FIELD_MIN_Y), int(FOOTBALL_FIELD_MAX_Y)+1):
            for x in range(int(FOOTBALL_FIELD_MIN_X), int(FOOTBALL_FIELD_MAX_X)+1):
                dmin = math.hypot(FOOTBALL_FIELD_MAX_X - FOOTBALL_FIELD_MIN_X - 1, FOOTBALL_FIELD_MAX_Y - FOOTBALL_FIELD_MIN_Y - 1)
                for temp_player in visual_players:
                    coord_x, coord_y = temp_player.get_position()
                    d = math.hypot(coord_x - x, coord_y - y)
                    if d < dmin:
                        dmin = d
                        dominant_player = temp_player
                if dominant_player in coordinates_per_player:
                    coordinates_per_player[dominant_player].append((x, y))
                else:
                    coordinates_per_player[dominant_player] = [(x, y)]
        # plotting side
        for player in coordinates_per_player:
            coordinates = coordinates_per_player[player]
            shapely_poly = geometry.MultiPoint(coordinates).convex_hull
            x, y = shapely_poly.exterior.coords.xy
            poly_patch = Polygon(zip(x, y), alpha=0.4, color=player.getObjectColor())
            self.ax.add_patch(poly_patch)
            self.regions.append(poly_patch)

    def remove(self):
        if self.regions:
            for region in self.regions:
                region.remove()
            self.regions = []

    def update(self, visual_idToPlayers):
        self.remove()
        self.draw(visual_idToPlayers.values())
