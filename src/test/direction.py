# from collections import defaultdict
#
# tree = lambda: defaultdict(tree)
#
# my_tree = tree()
# my_tree['a']['b']['c']['d']['e'] = 'whatever'
#
# if my_tree["a"]:
#     print "qq"


from math import atan2, degrees, pi

x1, y1 = 0,0
x2, y2 = 1,1


dx = x2 - x1
dy = y2 - y1
rads = atan2(-dy,dx)
rads %= 2*pi
degs = degrees(rads)

print degs