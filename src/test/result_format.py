import itertools

__author__ = 'emrullah'


# result = [[('Home Team', ('Ball Steal', 'Ball Lose', 'Ball Pass', 'Ball Ownership Time')), (1, (0, 0, 2, (0, 8, 0))), (2, (1, 0, 7, (0, 29, 0))), (5, (0, 0, 3, (0, 8, 0))), (7, (0, 0, 2, (0, 4, 0))), (9, (0, 0, 2, (0, 2, 0))), (11, (0, 0, 3, (0, 7, 0))), (77, (0, 0, 1, (0, 1, 0))), (16, (0, 1, 2, (1, 16, 0))), (22, (2, 2, 6, (1, 1, 0))), (88, (1, 0, 2, (0, 4, 0))), (25, (0, 0, 5, (0, 28, 0))), (-1, (0, 0, 0, (0, 0, 0)))]]
#
# for stats in result[0]:
#     # print stats
#     js, (ball_steal, ball_lose, ball_pass, ball_ownership_time) = stats
#     print  "%s %s %s %s %s\n" %(str(js).center(10), str(ball_steal).rjust(11), str(ball_lose).rjust(11),
#                                   str(ball_pass).rjust(11), str(ball_ownership_time).rjust(21))
#
#
# results = [[('Home Team', ('Ball Steal', 'Ball Lose', 'Ball Pass', 'Ball Ownership Time')), (1, (0, 0, 2, (0, 8, 0))), (2, (1, 0, 7, (0, 29, 0))), (5, (0, 0, 3, (0, 8, 0))), (7, (0, 0, 2, (0, 4, 0))), (9, (0, 0, 2, (0, 2, 0))), (11, (0, 0, 3, (0, 7, 0))), (77, (0, 0, 1, (0, 1, 0))), (16, (0, 1, 2, (1, 16, 0))), (22, (2, 2, 6, (1, 1, 0))), (88, (1, 0, 2, (0, 4, 0))), (25, (0, 0, 5, (0, 28, 0))), (-1, (0, 0, 0, (0, 0, 0)))], [('Away Team', ('Ball Steal', 'Ball Lose', 'Ball Pass', 'Ball Ownership Time')), (67, (0, 3, 1, (0, 12, 0))), (6, (0, 0, 2, (0, 6, 0))), (8, (0, 0, 1, (0, 4, 0))), (11, (0, 0, 0, (0, 0, 0))), (13, (2, 1, 3, (0, 18, 0))), (17, (0, 0, 1, (0, 26, 0))), (3, (0, 0, 0, (0, 0, 0))), (20, (0, 0, 1, (0, 4, 0))), (21, (0, 0, 0, (0, 0, 0))), (26, (1, 0, 1, (0, 1, 0))), (27, (0, 0, 0, (0, 0, 0))), (-1, (0, 0, 0, (0, 0, 0)))]]
#
# for (home_js, home_stats), (away_js, away_stats) in itertools.izip(results[0], results[1]):
#     ball_steal, ball_lose, ball_pass, ball_ownership_time = home_stats
#     ball_steal2, ball_lose2, ball_pass2, ball_ownership_time2 = away_stats
#     try:
#         print "%s %s %s %s %s | %s %s %s %s %s\n" %\
#              (str(ball_ownership_time).ljust(21), str(ball_pass).ljust(11),str(ball_lose).ljust(11),
#               str(ball_steal).ljust(12), str(home_js).center(9),
#               str(away_js).center(10), str(ball_steal2).rjust(11), str(ball_lose2).rjust(11),
#               str(ball_pass2).rjust(11), str(ball_ownership_time2).rjust(21))
#     except:
#         print "%s %s | %s %s\n" %(str(home_stats[::-1]).ljust(21), str(home_js).center(10),
#                                  str(away_js).center(10), str(away_stats).rjust(21))
#


results = [[('home', ('Total Coverage', 'Avg Coverage', 'Coverage %')), (7, (44078.482887140766, 146.92827629046923, 0.019990241672172684)), (25, (41145.09688873225, 137.15032296244084, 0.01865990788604637)), (22, (204092.35398199846, 680.3078466066615, 0.09255889069478389)), (16, (39457.14385514801, 131.52381285049336, 0.017894396306189574)), (9, (36189.900247456535, 120.63300082485512, 0.016412653173449674)), (88, (118106.32224430906, 393.68774081436356, 0.05356295793392701)), (1, (310644.25782579184, 1035.4808594193062, 0.14088174958085797)), (5, (101721.84101990823, 339.07280339969407, 0.04613235420404001)), (77, (71137.2677733544, 237.12422591118133, 0.03226179944369814)), (11, (37130.17242280029, 123.7672414093343, 0.016839080463855006)), (2, (224454.85556166916, 748.1828518722306, 0.10179358528873886))], [('away', ('Total Coverage', 'Avg Coverage', 'Coverage %')), (3, (30532.191913385774, 101.77397304461924, 0.013846799053689693)), (21, (45129.11296639388, 150.43037655464624, 0.020466717898591325)), (6, (20809.245685130165, 69.36415228376721, 0.00943729963044452)), (27, (83865.75923618603, 279.55253078728674, 0.038034357930243096)), (8, (36744.062178738095, 122.48020726246031, 0.016663973777205486)), (67, (350333.5991807321, 1167.7786639357737, 0.15888145087561548)), (17, (58859.455646879, 196.19818548959668, 0.026693630678856693)), (20, (137641.89536185333, 458.80631787284443, 0.06242262828201965)), (26, (47476.67513097249, 158.78486665877088, 0.021603383218880393)), (13, (86932.52981824224, 289.7750993941408, 0.03942518359103957)), (11, (48615.64519100373, 162.0521506366791, 0.022047911651248857))]]

for (home_js, home_stats), (away_js, away_stats) in itertools.izip(results[0], results[1]):
                total_coverage, average_coverage, coverage_percentage = home_stats
                total_coverage2, average_coverage2, coverage_percentage2 = away_stats
                try:
                    print  "%s %s %s %s | %s %s %s %s\n" %\
                         (("%.2f"%coverage_percentage).ljust(13), ("%.2f"%average_coverage).ljust(14),
                          ("%.2f"%total_coverage).ljust(15), str(home_js).center(5),
                          str(away_js).center(4), ("%.2f"%total_coverage2).rjust(16),
                          ("%.2f"%average_coverage2).rjust(14), ("%.2f"%coverage_percentage2).rjust(12))
                except:
                    print  "%s %s %s %s | %s %s %s %s\n" %\
                           (str(coverage_percentage).ljust(13), str(average_coverage).ljust(14),
                        str(total_coverage).ljust(15), str(home_js).center(5), str(away_js).center(4),
                        str(total_coverage2).rjust(16), str(average_coverage2).rjust(14),str(coverage_percentage2).rjust(12))