__author__ = 'aliuzun'

def getAC():
    with open("cfs.txt","r") as file:
        Q_number=0
        Wa={}
        for line in file:
            if line[0]=="Q":
                Q_number+=1
            if line[0]=="[":
                line=line.strip("[]\n")
                r1,r2,r3,w1,w2,w3,w4 = line.split(",")
                r1,r2,r3,w1,w2,w3,w4=int(r1),int(r2),int(r3),int(w1),int(w2),int(w3),int(w4)
                if Q_number not in Wa:
                    Wa[Q_number]=[r1,r2,r3,w1,w2,w3,w4]
                else:
                    Wa[Q_number]= [(Wa[Q_number][0]+r1)/2.0,(Wa[Q_number][1]+r2)/2.0,(Wa[Q_number][2]+r3)/2.0,(Wa[Q_number][3]+w1)/2.0,(Wa[Q_number][4]+w2)/2.0,
                                   (Wa[Q_number][5]+w3)/2.0,(Wa[Q_number][6]+w4)/2.0]
    return Wa

def getAScoreTT():
    with open("cfs.txt","r") as file:
        Q_number=0
        scoreTrain,scoreTest=0,0
        Wa={}
        for line in file:
            if line[0]=="Q":
                Q_number+=1
            if line[0]=="(":
                line=line.strip("()\n")
                r1,r2= line.split(",")
                r1,r2=int(r1),int(r2)
                if Q_number not in Wa:
                    Wa[Q_number]=[r1,r2]
                else:
                    val1,val2=(Wa[Q_number][0]+r1)/2.0,(Wa[Q_number][1]+r2)/2.0
                    Wa[Q_number]= [float("%0.2f"%val1),float("%0.2f"%val2)]
        return Wa

A= getAScoreTT()
B=getAC()
for k in B:
    print k,B[k],A[k]
# print getAC()

#
#
# 1 [29.99, 3.0] [9.999992370605206, 4.999023437496312, 19.999999999999254, 38.90233955657704, 81.74294861655932, 81.94629096934909, 87.00000000000001]
# 2 [30.99, 3.0] [9.9, 4.9, 8.9, 11.7, 32.4, 96.9, 39.0]
# 3 [30.99, 3.0] [9.9, 3.9, 17.0, 17.4, 38.8, 72.9, 41.0]
# 4 [30.0, 3.0] [9.9, 4.0, 12.9, 25.0, 55.2, 99.9, 74.0]
# 5 [30.99, 3.0] [9.9, 4.9, 8.9, 12.7, 32.8, 84.9, 20.9]
# 6 [29.99, 3.0] [9.9, 4.9, 19.9, 47.9, 99.9, 58.9, 18.0]
# 7 [30.99, 3.0] [9.9, 4.9, 8.9, 14.9, 37.9, 99.9, 15.0]
# 8 [29.99, 3.0] [9.9, 4.0, 19.9, 11.9, 48.9, 79.9, 66.9]
# 9 [30.99, 2.0] [9.9, 4.9, 19.9, 47.9, 99.9, 99.9, 87.9]
# 10 [31.99, 2.0] [9.9, 4.9, 9.9, 3.9, 13.0, 43.9, 39.0]
# 11 [30.0, 3.0] [9.9, 4.9, 19.9, 46.9, 96.8, 37.2, 49.9]
# 12 [30.99, 3.0] [9.9, 3.9, 16.9, 23.9, 53.9, 99.9, 23.0]
#
#
#
#
#
