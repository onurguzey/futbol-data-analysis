import os
from src.sentio.Parameters import DATA_BASE_DIR
from src.sentio.algorithms.Pass import Pass
from src.sentio.file_io.reader.ReaderBase import ReaderBase
from src.sentio.file_io.reader.XMLreader import XMLreader

__author__ = 'emrullah'




reader = XMLreader(os.path.join(DATA_BASE_DIR, 'output/sentio_data_new.xml'))
game_instances, slider_mapping = reader.parse()

q = {}
evaluate = Pass()
for game_instance in game_instances.getAllInstances():
    if game_instance.event and game_instance.event.isPassEvent():
        pass_event = game_instance.event.pass_event
        # if pass_event.isSuccessful():
        try:
            pass_source = pass_event.pass_source
            pass_target = pass_event.pass_target
            evaluate.teams = ReaderBase.divideIntoTeams(game_instance.players)
            temp_effectiveness = evaluate.effectiveness_withComponents(pass_source, pass_target)[-1]
            if (pass_source.getTypeName(), pass_source.jersey_number) in q:
                q[(pass_source.getTypeName(), pass_source.jersey_number)].append(temp_effectiveness)

            else:
                q[(pass_source.getTypeName(), pass_source.jersey_number)] = [temp_effectiveness]
        except:
            print "the algorithm is buggy"


for i in q:
    print i, sum(q[i])

